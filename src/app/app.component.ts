import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'auto-complete-search-ngx';

  options = ['One', 'Two', 'Three'];
  matchFirstChar = false;

  mode = new FormControl('');

  constructor() {

  }

  ngOnInit() {

  }

  onModeChange(param) {
    this.matchFirstChar = param.value == 'true';
  }


}
