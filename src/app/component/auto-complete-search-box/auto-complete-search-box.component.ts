import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-auto-complete-search-box',
  templateUrl: './auto-complete-search-box.component.html',
  styleUrls: ['./auto-complete-search-box.component.scss']
})
export class AutoCompleteSearchBoxComponent implements OnInit, OnChanges {

    searchKey = new FormControl();

    @Input() options: string[] = ['One', 'Two', 'Three'];
    @Input() matchFirstChar = false;

    filteredOptions: Observable<string[]>;

    constructor() {

    }

    ngOnInit() {
        this.initFilter();
    }

    ngOnChanges() {
        this.initFilter();
    }

    initFilter() {
        this.filteredOptions = this.searchKey.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.options.filter(option => {
            if (this.matchFirstChar) {
                return option.toLowerCase().indexOf(filterValue) === 0
            } else {
                return option.toLowerCase().includes(filterValue);
            }
        });
    }

}
